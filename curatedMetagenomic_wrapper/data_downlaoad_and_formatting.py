import argparse
import os
import argcomplete
import pandas as pd
import requests
from lxml import etree
from http.client import RemoteDisconnected
import time

info_text = """The curatedMetagenomic Data is a R Bioconductor Package collecting manually curated contextual metadata 
for human microbiome data. In order to feed primary resources (ENA-EMBL) with these information this script has been 
developed. It adds the ENA/SRA accession number in order to feed Contextual Clearing Data House (CDCH) by using the 
write API.\n Usage: \t1) denovo data creation: \n\t\tpython data_downlaoad_and_formatting.py \t\n2) to use a previously 
created curatedMetagenomic Data metadata file:\n\t\tpython data_downlaoad_and_formatting.py -m meta.csv\n 
Requirements:\n 
Python Environment:\n \t\t- Python 3.6\n \t\t- lxml\n \t\t- pandas\n
R Environment:\n \t\t- R 3.6.1\n \t\t- curatedMetagenomicData\n """


def input_options():
    parser = argparse.ArgumentParser(
        description=info_text,
        prefix_chars="-")
    parser.add_argument("-m", "--meta_file", type=str,
                        help="curatedMetagenomicData metadata file",
                        action="store", required=False, default=False)
    argcomplete.autocomplete(parser)
    return parser.parse_args()


def generate_df(meta_file):
    from rpy2.robjects import r
    from rpy2.robjects.packages import importr
    if meta_file is False and os.path.exists(meta_file) is False:
        meta_file = "meta.csv"
        importr('curatedMetagenomicData')
        utils = importr('utils')
        meta = r['combined_metadata']
        utils.write_csv(meta, meta_file)
    df = pd.read_csv(meta_file, low_memory=False)
    run_list_df = list(df["NCBI_accession"])
    return df, run_list_df


def ask_api(run_name):
    prj = "NA"
    sample = "NA"
    xml_file = "%s_run_data.xml" % run_name
    if os.path.exists(xml_file) and os.stat(xml_file)[6] != 0:
        prj, sample = parse_xml(xml_file)
    else:
        i = 0
        while i < 5:
            base_link = "https://www.ebi.ac.uk/ena/data/view/RUNACCESSION&display=xml"
            url_link = base_link.replace("RUNACCESSION", run_name)
            try:
                risposta = requests.get(url_link)
                if risposta.status_code == 200:
                    with open(xml_file, "w") as tmp:
                        tmp.write(risposta.text)
                    prj, sample = parse_xml(xml_file)
                    i = 6
            except RemoteDisconnected:
                time.spleep(120)
    return prj, sample


def parse_xml(xml_file):
    lettura = etree.parse(xml_file)
    prj = "NA"
    sample = "NA"
    for e in lettura.findall("//RUN_LINKS/RUN_LINK/XREF_LINK"):
        if e is not None:
            db = e.getchildren()[0].text.strip()
            acc = e.getchildren()[1].text.strip()
            # print(db, acc)
            if db == "ENA-SAMPLE":
                sample = acc
            if db == "ENA-STUDY":
                prj = acc
    return prj, sample


if __name__ == "__main__":
    info = input_options()
    metadata = info.meta_file
    dataframe, run_list = generate_df(metadata)
    project_list = []
    sample_list = []
    for run in run_list:
        p_l = []
        s_l = []
        print(run)
        if type(run) is str:
            for r_acc in list(map(str.strip, run.split(";"))):
                print(r_acc)
                progetto, campione = ask_api(r_acc)
                # print(progetto, campione)
                p_l.append(progetto)
                s_l.append(campione)
            project_list.append(";".join(p_l))
            sample_list.append(";".join(s_l))
        else:
            project_list.append("NA")
            sample_list.append("NA")
    dataframe["ENA-SAMPLE"] = sample_list
    dataframe["ENA-PROJECT"] = project_list
    dataframe.to_csv("curatedMetagenomicData_metadata_ENA_samples.tsv", sep="\t", index=False, date_format=str)
