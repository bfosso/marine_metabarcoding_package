# Marine metabarcoding Package

___This repository is part of the ELIXIR Italian node activities.___    
___Actually it is an alpha release.___  

## Rationale 

In order to make available annotated Meta-barcoding data regarding marine microbiome surveys, the Italian Node has developed a GitLab repository, called **Marine Metabarcoding package**, collecting annotated metabarcoding data.      
![data_clearingware_house.png](./data_clearingware_house.png)
*Figure 1. A schematic representation of the analytical workflow designed and developed to build the Marine Metabarcoding Package.*  

In Figure 1 is shown a schematic representation of the workflow designed and developed to retrieve and analyse the metabarcoding data.  
Initially, by using the ENA API, we selected about 200 bio-projects regarding the marine environment and following retrieved the metadata associated to both sample and sequencing runs.  
By taking in consideration in this first release we were principally interested in marine microbial communities, the samples metadata were filtered by collecting informations regarding the geographical coordinates, sampling depth and temperature.  
Following, the fastq raw data associated to amplicon experiments were locally downloaded.  
According to Callahan, et al. 2017 [PMID: 28731476], the amplicon sequence variants (ASV, also known as Exact sequence variants), offer a reliable approach to make metabarcoding data analysis “reusable across studies, reproducible in future data sets and not limited by incomplete reference databases”.  
By taking into account this statement, we denoised the raw data and inferred ASVs by using DADA2 [PMID: 27214047].  
Finally, the inferred ASVs were taxonomic annotated by using BioMaS [PMID: 26130132] and the release 132 of the SILVA database [PMID: 23193283] as taxonomic reference.  

## How to print the map of analysed samples
``` R
require(ggplot2)
require(ggmap)
require(maps)

t2 <- read.table("table2",sep = "\t", check.names = F)
t3 <- read.table("table3",sep = "\t", check.names = F, header = T)

latitude <- as.vector(t3$latitute)
longitude <- as.vector(t3$longitude)


mp <- NULL
mapWorld <- borders("world", colour="gray50", fill="gray50") # create a layer of borders
mp <- ggplot() +   mapWorld
mp <- mp + geom_point(aes(x=longitude, y=latitude) ,fill="deepskyblue",shape=25, size=2) 
mp
```
![sample_geographical_position.png](./sample_geographical_position.png)

## Repository Content
This repository contains three tables:  
- **Table1**: is a TSV files containing the following information:    
    - ProjectID: Project accession number;  
    - Amplicon Run: number of available Amplicon sequencing run(s);  
    - Processed Run: number of analysed Amplicon sequencing run(s);   
    - Completeness: percentage of analysed data;  
    - Taxonomy_table: taxonomy table for the project samples;  
    - ASV_table: ASV table for the project samples  
- **Table1**: is a TSV files containing the following information:  
    - study_accession: Project accession number;  
    - sample_accession: sample ID;  
    - secondary_sample_accession: secondary sample ID ;  
    - experiment_accession: experiment ID;  
    - run_accession: run ID;  
    - library_layouy: SINGLE- or PAIRED- ends sequencing;  
    - instrument_model.  
- **Table1**: is a TSV files containing the following information about the sample metadata:  
    - ProjectID: Project accession number;  
    - secondary_sample_accession;  
    - longitude;  
    - latitute;  
    - depth: sampling depth;  
    - instrument;  
    - biome;  
    - temperature.  

The **prj-data** contains the project-specific taxon and the ASVs tables. 

## curatedMetagenomic Data wrapper
The **curatedMetagenomic Data** [PMID:29088129](https://pubmed.ncbi.nlm.nih.gov/29088129-accessible-curated-metagenomic-data-through-experimenthub/?from_term=Pasolli%2C+Edoardo%5BAuthor%5D&from_pos=9) is a Bioconductor package collecting manually curated contextual information, taxonomical and functional profiles associated to human microbiome data in more than 10,000 samples (version 1.16.0).  
In particular, the manually curated metadata offers a way to increase the annotation level of data available in primary resources, such as the ENA-EMBL, in full agreement with the FAIR (Findability, Accessibility, Interoperability and Reproducibility).  
The curatedMetagenomic Data contextual information are available as R dataframe and actually lacks a direct link to the [Biosamples](https://www.ebi.ac.uk/biosamples/) database and only the information about the **ENA/SRA** repositories run accession number (e.g. ERR, SRR) are available.  
By using the run accession number it is possible to obtain the relative sample (ERS, SRS) accession number, which is usable to query the Biosamples database.  
Finally, the Sample accession allows to submit metadata to the Contextual Data ClearingHouse (CDCH) (API-service).  

![schema](curatedMetagenomic_wrapper/cMD_ro_CDCH.png)
*Figure 1. A schematic representation of the analytical workflow designed and developed to prepare the contextual data in curatedMetagenomic Data R package for the submission to the CDCH.*  

In *Figure 1* is shown a schematic representation of the workflow designed and developed as a Python 3.6 script to retrieve the Sample accession number.  
In particular, the curatedMetagenomic Data metadata are locally stored and converted in a Pandas dataframe.  
The advantage of this data structure relies in its flexibility allowing to extract and insert information.  
Indeed, the list of run accession number is obtained and used to query the ENA database by using the relative API and download data as an XML file.  
Following the XML files is parsed out in order to extract the sample and the project accession numbers.  
Finally, two columns are added to the Pandas dataframe in order to annotate for each available sample the relative ENA sample and project accession numbers ( if any of the information is not available an NA is inserted).  
In conclusion, as standard datatype a TSV file was proposed allowing to store all the needed information to feed the CDCH.  

### Requirements
The data_downlaoad_and_formatting.py has been developed and testes by using Python 3.6 and R 3.6.1.  
Python Packages:  
- lxml  
- pandas  
R Environment:  
- curatedMetagenomicData   